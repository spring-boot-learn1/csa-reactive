package com.quovadis.csa.reactive.service;

import com.quovadis.csa.reactive.model.MeetupDocument;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MeetupService {
    Flux<MeetupDocument> searchMeetByName(String name);

    Mono<MeetupDocument> saveMeetup(MeetupDocument meetup);
}
