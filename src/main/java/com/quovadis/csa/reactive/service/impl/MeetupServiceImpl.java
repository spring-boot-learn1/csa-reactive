package com.quovadis.csa.reactive.service.impl;

import com.quovadis.csa.reactive.external.service.WeatherExternalService;
import com.quovadis.csa.reactive.model.MeetupDocument;
import com.quovadis.csa.reactive.model.WeatherDocument;
import com.quovadis.csa.reactive.repository.MeetupRepository;
import com.quovadis.csa.reactive.service.MeetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MeetupServiceImpl implements MeetupService {

    @Autowired
    WeatherExternalService weatherExternalService;

    @Autowired
    MeetupRepository meetupRepository;

    @Override
    public Flux<MeetupDocument> searchMeetByName(String name) {
        return meetupRepository.findAll();
    }

    @Override
    public Mono<MeetupDocument> saveMeetup(MeetupDocument meetup) {


        weatherExternalService.searchWeather(meetup.getLocation(), meetup.getMeetupDate());

        return null;
    }
}
