package com.quovadis.csa.reactive.external;

import com.quovadis.csa.reactive.external.model.AllContentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Configuration
public class TestWebClient {

    Logger logger = LoggerFactory.getLogger(TestWebClient.class);

    WebClient webClient;

    public TestWebClient(WebClient webClient){
        this.webClient = webClient;
    }

    public void testGet(){

        this.webClient = webClient
                .mutate()
                .baseUrl("http://api.openweathermap.org/data/2.5")
                .build();

        Mono<AllContentResponse> responseMono = this.webClient
                .get()
                .uri("/forecast?q=mendoza&lang=es&units=metric&&appid=8edf7d519bedc2906c197622d9ea3257")
                .retrieve()
                .bodyToMono(AllContentResponse.class)
                .doOnSuccess(this::consumerAllContent)
                .onErrorStop()
                .onErrorReturn(new AllContentResponse());

        responseMono.subscribe();
    }

    private void consumerAllContent(AllContentResponse allContentResponse) {
        logger.info("consumerAllContent");
        logger.info("Response: {}", allContentResponse.toString());
    }

}
