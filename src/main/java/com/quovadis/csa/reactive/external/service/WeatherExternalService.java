package com.quovadis.csa.reactive.external.service;

import com.quovadis.csa.reactive.model.LocationDocument;

import java.time.LocalDateTime;

public interface WeatherExternalService {
    void searchWeather(LocationDocument location, LocalDateTime meetDate);
}
