package com.quovadis.csa.reactive.external.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class AllContentResponse {

    int cod;
    double message;
    int cnt;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    @Override
    public String toString() {
        return "AllContentResponse{" +
                "cod=" + cod +
                ", message=" + message +
                ", cnt=" + cnt +
                '}';
    }
}
