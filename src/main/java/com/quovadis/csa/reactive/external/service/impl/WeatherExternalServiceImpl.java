package com.quovadis.csa.reactive.external.service.impl;

import com.quovadis.csa.reactive.external.TestWebClient;
import com.quovadis.csa.reactive.external.model.AllContentResponse;
import com.quovadis.csa.reactive.external.service.WeatherExternalService;
import com.quovadis.csa.reactive.model.LocationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
public class WeatherExternalServiceImpl implements WeatherExternalService {

    Logger logger = LoggerFactory.getLogger(WeatherExternalServiceImpl.class);

    @Autowired
    TestWebClient testWebClient;

    @Override
    public void searchWeather(LocationDocument location, LocalDateTime meetupDate) {

        if (!isInRangeWeatherDay(meetupDate)){
            //return null;
        }

        testWebClient.testGet();

    }

    private Mono<AllContentResponse> proccess(AllContentResponse allContentResponse) {


        System.out.println(allContentResponse.toString());
        return null;
    }


    private boolean isInRangeWeatherDay(LocalDateTime meetupDate){
        int dayNow = LocalDateTime.now().getDayOfYear();
        int dayMeetup = meetupDate.getDayOfYear();

        return 5 > (dayMeetup - dayNow);
    }
}
