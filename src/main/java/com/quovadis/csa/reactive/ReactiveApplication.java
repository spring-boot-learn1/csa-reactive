package com.quovadis.csa.reactive;

import com.quovadis.csa.reactive.external.model.AllContentResponse;
import com.quovadis.csa.reactive.model.LocationDocument;
import com.quovadis.csa.reactive.model.MeetupDocument;
import com.quovadis.csa.reactive.model.UserDocument;
import com.quovadis.csa.reactive.model.WeatherDocument;
import com.quovadis.csa.reactive.repository.MeetupRepository;
import io.netty.util.internal.StringUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ReactiveApplication {

	WebClient webClient;

	public static void main(String[] args) {
		SpringApplication.run(ReactiveApplication.class, args);
	}

//	//Keep the rest of the code untouched. Just add the following method
//	@Bean
//	CommandLineRunner init(ReactiveMongoOperations operations, MeetupRepository meetupRepository) {
//		return args -> {
//
//			UserDocument user = new UserDocument("guso007@gmail.com");
//			LocationDocument location = new LocationDocument("Julio LeParck");
//			WeatherDocument weather = new WeatherDocument("Soleado", 28.0D, LocalDateTime.now());
//
//			List<UserDocument> usersAssist = Arrays.asList(user);
//
//			MeetupDocument meetupDocument = new MeetupDocument("WebFlux para todos!", "Programación reactiva",
//					location, weather, usersAssist);
//
//			meetupRepository
//					.deleteAll()
//					//.findAll()
//					//.thenMany(Flux.empty())
//					.thenMany(
//						Flux.just(meetupDocument)
//							.flatMap(meetupRepository::save))
//					.thenMany(meetupRepository.findAll())
//					.subscribe(System.out::println);
//		};
//	}

	@Bean
	CommandLineRunner init(WebClient webClient){

		return args -> {

			this.webClient = webClient
					.mutate()
					.baseUrl("http://api.openweathermap.org/data/2.5")
					.build();

			Mono<AllContentResponse> responseMono = this.webClient
					.get()
					.uri("/forecast/daily?q=mendoza&cnt=5&lang=es&units=metric&&appid=8edf7d519bedc2906c197622d9ea3257")
					.accept(MediaType.APPLICATION_JSON)
					.retrieve()
					.bodyToMono(AllContentResponse.class)
					.onErrorStop()
					.onErrorReturn(new AllContentResponse());

			responseMono.subscribe();

			System.out.println(responseMono.block().getCod());
		};
	}
}
