package com.quovadis.csa.reactive.repository;

import com.quovadis.csa.reactive.model.MeetupDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface MeetupRepository extends ReactiveCrudRepository<MeetupDocument,String> {
}
