package com.quovadis.csa.reactive.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "meetup")
public class MeetupDocument {

    @Id
    public ObjectId _id;

    String name;

    String description;

    LocationDocument location;

    WeatherDocument weatherDay;

    List<UserDocument> usersAssist;

    LocalDateTime meetupDate;

    LocalDateTime createdDate;

    public MeetupDocument(){
        this.meetupDate = LocalDateTime.now().plusDays(2);
    }

    public MeetupDocument(String name){
        this.name = name;
        this.meetupDate = LocalDateTime.now().plusDays(2);
    }

    public MeetupDocument(String name, String description, LocationDocument location, WeatherDocument weatherDay, List<UserDocument> usersAssist) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.weatherDay = weatherDay;
        this.usersAssist = usersAssist;
        this.meetupDate = LocalDateTime.now().plusDays(2);
        this.createdDate = LocalDateTime.now();
    }


    //Getters and setters
    // ObjectId needs to be converted to string
    public String get_id() { return _id.toHexString(); }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationDocument getLocation() {
        return location;
    }

    public void setLocation(LocationDocument location) {
        this.location = location;
    }

    public WeatherDocument getWeatherDay() {
        return weatherDay;
    }

    public void setWeatherDay(WeatherDocument weatherDay) {
        this.weatherDay = weatherDay;
    }

    public List<UserDocument> getUsersAssist() {
        return usersAssist;
    }

    public void setUsersAssist(List<UserDocument> usersAssist) {
        this.usersAssist = usersAssist;
    }

    public LocalDateTime getMeetupDate() {
        return meetupDate;
    }

    public void setMeetupDate(LocalDateTime meetupDate) {
        this.meetupDate = meetupDate;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "MeetupDocument{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", location=" + location +
                ", weatherDay=" + weatherDay +
                ", usersAssist=" + usersAssist +
                ", meetDate=" + meetupDate +
                ", createdDate=" + createdDate +
                '}';
    }
}
