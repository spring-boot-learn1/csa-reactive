package com.quovadis.csa.reactive.model;

import io.netty.util.internal.StringUtil;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "location")
public class LocationDocument {

    @Id
    public ObjectId _id;

    String address;

    public LocationDocument(String address) {
        this.address = address;
    }

    //Getters and setters
    // ObjectId needs to be converted to string
    public String get_id() {
        return (_id != null) ? _id.toHexString() : StringUtil.EMPTY_STRING;
    }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
