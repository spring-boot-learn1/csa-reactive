package com.quovadis.csa.reactive.model;

import io.netty.util.internal.StringUtil;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
public class UserDocument {

    @Id
    public ObjectId _id;

    String username;

    public UserDocument(String username) {
        this.username = username;
    }

    //Getters and setters
    // ObjectId needs to be converted to string
    public String get_id() { return (_id != null) ? _id.toHexString() : StringUtil.EMPTY_STRING; }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
