package com.quovadis.csa.reactive.model;

import io.netty.util.internal.StringUtil;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "weather")
public class WeatherDocument {

    @Id
    public ObjectId _id;

    String forecast;

    Double temperature;

    LocalDateTime forecastDate;

    public WeatherDocument(String forecast, Double temperature, LocalDateTime forecastDate) {
        this.forecast = forecast;
        this.temperature = temperature;
        this.forecastDate = forecastDate;
    }

    //Getters and setters
    // ObjectId needs to be converted to string
    public String get_id() { return (_id != null) ? _id.toHexString() : StringUtil.EMPTY_STRING; }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public LocalDateTime getForecastDate() {
        return forecastDate;
    }

    public void setForecastDate(LocalDateTime forecastDate) {
        this.forecastDate = forecastDate;
    }
}
