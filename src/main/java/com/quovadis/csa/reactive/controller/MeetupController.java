package com.quovadis.csa.reactive.controller;

import com.quovadis.csa.reactive.model.MeetupDocument;
import com.quovadis.csa.reactive.service.MeetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping(path = "api/v1/meetup")
public class MeetupController {

    Logger logger = LoggerFactory.getLogger(MeetupController.class);

    @Autowired
    MeetupService meetService;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MeetupDocument> searchMeetupByName(@RequestParam(required = false) String name){
        logger.info("searchMeetup: {}", name);
        Mono<MeetupDocument> resultA = meetService.saveMeetup(new MeetupDocument());
        Flux<MeetupDocument> result = meetService.searchMeetByName(name);

        return result.delayElements(Duration.ofMillis(1l));
    }

    @PostMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<MeetupDocument> saveMeetup(@RequestBody MeetupDocument meetup){
        logger.info("saveMeetup: {}", meetup);
        Mono<MeetupDocument> result = meetService.saveMeetup(meetup);

        return result;
    }
}
